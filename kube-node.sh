#!/bin/bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
sudo usermod -aG docker ubuntu
hn=`curl http://169.254.169.254/latest/meta-data/local-hostname`
sudo hostnamectl set-hostname $hn
sudo apt-get -y install nfs-common awscli python cloud-utils
sudo service nfs start
su ubuntu -c "aws configure set region us-east-1"
sudo apt-get update && sudo apt-get install -y apt-transport-https curl 
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet=1.18.0-00 kubeadm=1.18.0-00 kubectl=1.18.0-00
sudo apt-mark hold kubelet kubeadm kubectl
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart ssh
sudo usermod -p '$1$d2OEPnWF$XGDpAkk1zxFEccpTORP4x/' ubuntu
su ubuntu -c "aws configure set region us-east-1"
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf