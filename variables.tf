variable "profile" {
  default = "gl-project"
}

variable "region" {
  default = "us-east-1"
}

variable "az" {
  type    = "list"
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "vpc-cidr" {
  default = "195.10.0.0/20"
  
}


variable "kube-pub1-cidr" {
  default = "195.10.0.0/24"
  
}

variable "kube-pub2-cidr" {
  default = "195.10.1.0/24"
  
}

variable "kube-pub3-cidr" {
  default = "195.10.2.0/24"
  
}

variable "kube-priv1-cidr" {
  default = "195.10.3.0/24"
  
}

variable "kube-priv2-cidr" {
  default = "195.10.4.0/24"
}

variable "kube-priv3-cidr" {
  default = "195.10.5.0/24"
}

variable "internet_cidr" {
  default = "0.0.0.0/0" 
}

variable "instance_type" {
  default = ["t2.micro", "t2.xlarge", "t2.large"]    
}

variable "ami" {

  default = "ami-085925f297f89fce1"
  
}

variable "keypair" {

  default = "/Users/praveen/.pem/prvn-key.pem"
  
}

variable "zone_id" {
  default = "Z05089832XCOQZNTZNQR6"
}