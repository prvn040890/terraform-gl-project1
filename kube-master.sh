#!/bin/bash
curl -fsSL https://get.docker.com -o get-docker.sh 
sh get-docker.sh
sudo usermod -aG docker ubuntu
# shellcheck disable=SC2006
hn=`curl http://169.254.169.254/latest/meta-data/local-hostname`
sudo hostnamectl set-hostname $hn
mkdir -p /home/ubuntu/.kube
sudo apt-get update && sudo apt-get install -y apt-transport-https curl awscli python sshpass nfs-common
sudo service nfs start
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet=1.18.0-00 kubeadm=1.18.0-00 kubectl=1.18.0-00
sudo apt-mark hold kubelet kubeadm kubectl
# shellcheck disable=SC1090
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> /home/ubuntu/.bashrc
aws configure set region us-east-1
su ubuntu -c "aws configure set region us-east-1"
aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag-value,Values=*Node*" --query "Reservations[*].Instances[*].[PrivateIpAddress]" --output text > /home/ubuntu/nodes.txt && chown 1000:1000 /home/ubuntu/nodes.txt
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart ssh
sudo usermod -p '$1$d2OEPnWF$XGDpAkk1zxFEccpTORP4x/' ubuntu
su - ubuntu -c "ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa 2>/dev/null <<< y >/dev/null"
sudo modprobe br_netfilter
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
cd /home/ubuntu/ && git clone https://gitlab.com/prvn040890/kubernetes-yaml.git && chown 1000:1000 -R /home/ubuntu/kubernetes-yaml
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/spass.sh"
sudo kubeadm init --config=/home/ubuntu/kubernetes-yaml/aws.yaml > /tmp/install.txt && sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config && sudo chown 1000:1000 /home/ubuntu/.kube/config && sudo chown 1000:1000 /tmp/install.txt
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/gen-join.sh" 
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/scp-join.sh"
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/join.sh"
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/kube-config.sh"
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/efs.sh"
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
su ubuntu -c "sh /home/ubuntu/kubernetes-yaml/scripts/kube-init.sh"
