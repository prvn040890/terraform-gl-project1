provider "aws" {
  region = "${var.region}"
  profile= "${var.profile}"
}

resource "aws_vpc" "kube-vpc" {
  cidr_block = "${var.vpc-cidr}"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "kube-vpc"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
}

resource "aws_subnet" "kube-public-1" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-pub1-cidr}"
  availability_zone = "${var.az[0]}"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "kube_public_1"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
}

resource "aws_subnet" "kube-public-2" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-pub2-cidr}"
  availability_zone = "${var.az[1]}"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "kube_public_2"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
}

resource "aws_subnet" "kube-public-3" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-pub3-cidr}"
  availability_zone = "${var.az[2]}"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "kube_public_3"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
}

resource "aws_subnet" "kube-private-1" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-priv1-cidr}"
  availability_zone = "${var.az[0]}"

  tags = {
    Name = "kube_private_1"
 
  }
}

resource "aws_subnet" "kube-private-2" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-priv2-cidr}"
  availability_zone = "${var.az[1]}"

  tags = {
    Name = "kube_private_2"
 
  }
}

resource "aws_subnet" "kube-private-3" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  cidr_block = "${var.kube-priv3-cidr}"
  availability_zone = "${var.az[2]}"

  tags = {
    Name = "kube_private_3"
  }
}

resource "aws_internet_gateway" "kube-IGW" {
  vpc_id = "${aws_vpc.kube-vpc.id}"

  tags = {
    Name = "kube_IGW"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
}


resource "aws_eip" "kube-eip" {
  
  tags = {
    Name = "EIP_NatGateway"
  }
}

resource "aws_nat_gateway" "kube-NAT-GW" {
  allocation_id = "${aws_eip.kube-eip.id}"
    subnet_id = "${aws_subnet.kube-public-3.id}"

    tags = {
      Name = "kube_NAT_GW"
     "kubernetes.io/cluster/kubernetes" = "shared"
     "kubernetes.io/role/elb"  =  ""
     "kubernetes.io/role/alb-ingress"   =  ""
    }
}

resource "aws_route_table" "kube-public-RT" {

  vpc_id = "${aws_vpc.kube-vpc.id}"

  route {
    cidr_block = "${var.internet_cidr}"
    gateway_id = "${aws_internet_gateway.kube-IGW.id}"
  }

  tags = {
    Name = "kube_Public_Route_Table"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""
  }
  
}

resource "aws_route_table" "kube-private-RT" {
  vpc_id = "${aws_vpc.kube-vpc.id}"
  
  route {
    cidr_block = "${var.internet_cidr}"
    nat_gateway_id = "${aws_nat_gateway.kube-NAT-GW.id}"
  }

  tags = {
    Name = "kube_Private_Route_Table"
  }
  
}

resource "aws_route_table_association" "kube-public-1-RTAS" {
  subnet_id = "${aws_subnet.kube-public-1.id}"
  route_table_id = "${aws_route_table.kube-public-RT.id}"
}

resource "aws_route_table_association" "kube-public-2-RTAS" {
  subnet_id = "${aws_subnet.kube-public-2.id}"
  route_table_id = "${aws_route_table.kube-public-RT.id}"
}

resource "aws_route_table_association" "kube-public-3-RTAS" {
  subnet_id = "${aws_subnet.kube-public-3.id}"
  route_table_id = "${aws_route_table.kube-public-RT.id}"
}

resource "aws_route_table_association" "kube-private-1-RTAS" {
  subnet_id = "${aws_subnet.kube-private-1.id}"
  route_table_id = "${aws_route_table.kube-private-RT.id}"
  
}

resource "aws_route_table_association" "kube-private-2-RTAS" {
  subnet_id = "${aws_subnet.kube-private-2.id}"
  route_table_id = "${aws_route_table.kube-private-RT.id}"
  
}

resource "aws_route_table_association" "kube-private-3-RTAS" {
  subnet_id = "${aws_subnet.kube-private-3.id}"
  route_table_id = "${aws_route_table.kube-private-RT.id}"
  
}

resource "aws_security_group" "Jump-Box-SG" {
  name = "Jump_Box_SG"
  description = "Jump Box Security Group"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  ingress {
    description = "Opens SSH port for the Jump Box"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.internet_cidr}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol =  "-1"
    cidr_blocks = ["${var.internet_cidr}"]
  }

  tags = {
    Name = "Jump_Box_SG"
  }
}

resource "aws_security_group" "kube-master-SG" {
  name = "Kube_Master_SG"
  description = "Kubernetes Master node Security Group"
  vpc_id = "${aws_vpc.kube-vpc.id}"
  ingress {
    description = "Opens Port for Kubernetes API Server"
    from_port = 6443
    to_port = 6443
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }
  ingress {
    description = "Opens Port for etcd API"
    from_port = 2379
    to_port = 2380
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }
  ingress {
    description = "Opens Port for Kublet API"
    from_port = 10250
    to_port = 10250
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }


  ingress {
    description = "Opens Port for Kube Scheduler"
    from_port = 10251
    to_port = 10251
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }
  ingress {
    description = "Opens Port for Kube Controller"
    from_port = 10252
    to_port = 10252
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }

  ingress {
    description = "Opens Port for port for coe DNS"
    from_port = 6783
    to_port = 6783
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }

  ingress {
    description = "Opens Port for SSH connectivity"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}",
      "${aws_subnet.kube-public-1.cidr_block}",
      "${aws_subnet.kube-public-2.cidr_block}",
      "${aws_subnet.kube-public-3.cidr_block}"]

  }
 
  egress {
    from_port = 0
    to_port = 0
    protocol =  "-1"
    cidr_blocks = ["${var.internet_cidr}"]
  }

  tags = {
    Name = "Kube-Master-SG"
  }
}

resource "aws_security_group" "kube-lb-SG" {
  name = "kube_lb_SG"
  description = "Security group for the Load balancer"
  vpc_id = aws_vpc.kube-vpc.id

  ingress {
    description = "Opens HTTP port for the application connectivity"
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["${var.internet_cidr}"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["${var.internet_cidr}"]
  }

  tags = {
  Name = "Kube-LB-SG"
  }
}


resource "aws_security_group" "kube-worker-SG" {
  name = "Kube_Worker_SG"
  description = "Kubernetes Worker node Security Group"
  vpc_id = "${aws_vpc.kube-vpc.id}"
  

  ingress {
    description = "Opens Port for Kublet"
    from_port = 10250
    to_port = 10250
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }
  ingress {
    description = "Opens the node ports for Kubenetes external connections"
    from_port = 30000
    to_port = 32767
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]

  }

  ingress {
    description = "Opens SSH port for Worker nodes"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}",
      "${aws_subnet.kube-public-1.cidr_block}",
      "${aws_subnet.kube-public-2.cidr_block}",
      "${aws_subnet.kube-public-3.cidr_block}"]
  }

  ingress {
    description = "Opens Port for port for coe DNS"
    from_port = 6783
    to_port = 6783
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }

  ingress {
    description = "Allows Load balancer to communicate with worker nodes"
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = ["${aws_security_group.kube-lb-SG.id}"]
  }


  egress {
    from_port = 0
    to_port = 0
    protocol =  "-1"
    cidr_blocks = ["${var.internet_cidr}"]
  }



  tags = {
    Name = "Kube-worker-SG"
  }
}

resource "aws_security_group" "efs-SG" {
  name = "efs-security-sg"
  description = "Security group for EFS"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  ingress {
    description = "Opens NFS traffic between the Worker nodes"
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = [
      "${aws_subnet.kube-private-1.cidr_block}",
      "${aws_subnet.kube-private-2.cidr_block}",
      "${aws_subnet.kube-private-3.cidr_block}"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol =  "-1"
    cidr_blocks = ["${var.internet_cidr}"]

  }

  tags = {
    Name = "kube-efs-SG"
  }
  
}



resource "aws_instance" "Jump_Server" {
  ami = "${var.ami}"
  key_name = "prvn"
  instance_type = "${var.instance_type[0]}"
  subnet_id = "${aws_subnet.kube-public-1.id}"
  vpc_security_group_ids = ["${aws_security_group.Jump-Box-SG.id}"]


  provisioner "file" {

  source = "/Users/praveen/.pem/prvn-key.pem"
  destination = "/home/ubuntu/prvn-key.pem"

  connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/Users/praveen/.pem/prvn-key.pem")}"
      host = "${aws_instance.Jump_Server.public_ip}"
    }
  }


  tags = {
    Name = "Jump Server"
  }
  
}

resource "aws_instance" "kube-master01" {
  ami = "${var.ami}"
  key_name = "prvn"
  instance_type = "${var.instance_type[1]}"
  subnet_id = "${aws_subnet.kube-private-1.id}"
  vpc_security_group_ids = [
    "${aws_security_group.kube-master-SG.id}"]
  iam_instance_profile = "kube-IAM"
  user_data = "${file("kube-master.sh")}"


  tags = {
    Name = "Kube-Master01"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb" = ""
    "kubernetes.io/role/alb-ingress" = ""

  }

  depends_on = [
    "aws_nat_gateway.kube-NAT-GW",
    "aws_instance.kube-node01",
    "aws_instance.kube-node02",
    "aws_instance.kube-node03"]

}

resource "aws_instance" "kube-node01" {
  ami = "${var.ami}"
  key_name = "prvn"
  instance_type = "${var.instance_type[2]}"
  subnet_id = "${aws_subnet.kube-private-1.id}"
  vpc_security_group_ids = ["${aws_security_group.kube-worker-SG.id}"]
  iam_instance_profile = "kube-IAM"
  user_data = "${file("kube-node.sh")}"

  tags = {
    Name = "Kube-Node01"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""

  }

  depends_on = ["aws_nat_gateway.kube-NAT-GW"]

}

resource "aws_instance" "kube-node02" {
  ami = "${var.ami}"
  key_name = "prvn"
  instance_type = "${var.instance_type[2]}"
  subnet_id = "${aws_subnet.kube-private-2.id}"
  vpc_security_group_ids = ["${aws_security_group.kube-worker-SG.id}"]
  iam_instance_profile = "kube-IAM"
  user_data = "${file("kube-node.sh")}"

  tags = {
    Name = "Kube-Node02"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""

  }

  depends_on = ["aws_nat_gateway.kube-NAT-GW"]

}

resource "aws_instance" "kube-node03" {
  ami = "${var.ami}"
  key_name = "prvn"
  instance_type = "${var.instance_type[2]}"
  subnet_id = "${aws_subnet.kube-private-3.id}"
  vpc_security_group_ids = ["${aws_security_group.kube-worker-SG.id}"]
  iam_instance_profile = "kube-IAM"
  user_data = "${file("kube-node.sh")}"

  tags = {
    Name = "Kube-Node03"
    "kubernetes.io/cluster/kubernetes" = "shared"
    "kubernetes.io/role/elb"  =  ""
    "kubernetes.io/role/alb-ingress"   =  ""

  }

  depends_on = ["aws_nat_gateway.kube-NAT-GW"]

}


resource "aws_efs_file_system" "kube-efs" {
  creation_token = "kube-efs"
  performance_mode = "maxIO"

  tags = {
    Name = "Kube-efs"
  }
}

resource "aws_efs_mount_target" "kube-efs-target-priv1" {
  file_system_id = "${aws_efs_file_system.kube-efs.id}"
  security_groups = ["${aws_security_group.efs-SG.id}"]
  subnet_id = "${aws_subnet.kube-private-1.id}"
}

resource "aws_efs_mount_target" "kube-efs-target-priv2" {
  file_system_id = "${aws_efs_file_system.kube-efs.id}"
  security_groups = ["${aws_security_group.efs-SG.id}"]
  subnet_id = "${aws_subnet.kube-private-2.id}"
}

resource "aws_efs_mount_target" "kube-efs-target-priv3" {
  file_system_id = "${aws_efs_file_system.kube-efs.id}"
  security_groups = ["${aws_security_group.efs-SG.id}"]
  subnet_id = "${aws_subnet.kube-private-3.id}"
}

resource "aws_lb" "kube-lb" {
  name = "kube-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = ["${aws_security_group.kube-lb-SG.id}"]
  subnets = ["${aws_subnet.kube-public-1.id}","${aws_subnet.kube-public-2.id}","${aws_subnet.kube-public-3.id}"]
  enable_cross_zone_load_balancing = true

  tags = {
    Name = "Kube-lb"
  }

  depends_on = [
    "aws_instance.kube-node01",
    "aws_instance.kube-node02",
    "aws_instance.kube-node03"]

}

resource "aws_alb_listener" "kube-lb-listener" {
  load_balancer_arn = "${aws_lb.kube-lb.arn}"
  port = 80
  protocol = "HTTP"
  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/html"
      message_body = "Page Not Found"
      status_code = 404
    }

  }

}

resource "aws_alb_target_group" "kube-web-tg" {
  name = "kube-web-tg"
  port = 30080
  protocol = "HTTP"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  stickiness {
    type = "lb_cookie"
    cookie_duration = 3600
    enabled = true
  }

  health_check {
    path = "/"
    port = 30080
    matcher = "200"
  }
}

resource "aws_alb_target_group" "kube-web-test-tg" {
  name = "kube-web-test-tg"
  port = 30081
  protocol = "HTTP"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  stickiness {
    type = "lb_cookie"
    cookie_duration = 3600
    enabled = true
  }

  health_check {
    path = "/"
    port = 30081
    matcher = "200"
  }
}

resource "aws_alb_target_group" "kube-jenkins-tg" {
  name = "kube-jenkins-tg"
  port = 30808
  protocol = "HTTP"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  stickiness {
    type = "lb_cookie"
    cookie_duration = 3600
    enabled = true
  }

  health_check {
    path = "/login"
    protocol = "HTTP"
    port = 30808
    matcher = "200"
  }
}

resource "aws_alb_target_group" "kube-dashboard-tg" {
  name = "kube-dashboard-tg"
  port = 30808
  protocol = "HTTP"
  vpc_id = "${aws_vpc.kube-vpc.id}"

  stickiness {
    type = "lb_cookie"
    cookie_duration = 3600
    enabled = true
  }

  health_check {
    path = "/"
    protocol = "HTTP"
    port = 30050
    matcher = "200"
  }
}

resource "aws_alb_target_group_attachment" "kube-web-tg-attach-node01" {
  target_group_arn = "${aws_alb_target_group.kube-web-tg.arn}"
  target_id = "${aws_instance.kube-node01.id}"
  port = 30080
}

resource "aws_alb_target_group_attachment" "kube-web-tg-attach-node02" {
  target_group_arn = "${aws_alb_target_group.kube-web-tg.arn}"
  target_id = "${aws_instance.kube-node02.id}"
  port = 30080
}

resource "aws_alb_target_group_attachment" "kube-web-tg-attach-node03" {
  target_group_arn = "${aws_alb_target_group.kube-web-tg.arn}"
  target_id = "${aws_instance.kube-node03.id}"
  port = 30080
}

resource "aws_alb_target_group_attachment" "kube-web-test-tg-attach-node01" {
  target_group_arn = "${aws_alb_target_group.kube-web-test-tg.arn}"
  target_id = "${aws_instance.kube-node01.id}"
  port = 30081
}

resource "aws_alb_target_group_attachment" "kube-web-test-tg-attach-node02" {
  target_group_arn = "${aws_alb_target_group.kube-web-test-tg.arn}"
  target_id = "${aws_instance.kube-node02.id}"
  port = 30081
}

resource "aws_alb_target_group_attachment" "kube-web-test-tg-attach-node03" {
  target_group_arn = "${aws_alb_target_group.kube-web-test-tg.arn}"
  target_id = "${aws_instance.kube-node03.id}"
  port = 30081
}

resource "aws_alb_target_group_attachment" "kube-jenkins-tg-attach-node01" {
  target_group_arn = "${aws_alb_target_group.kube-jenkins-tg.arn}"
  target_id = "${aws_instance.kube-node01.id}"
  port = 30808
}

resource "aws_alb_target_group_attachment" "kube-jenkins-tg-attach-node02" {
  target_group_arn = "${aws_alb_target_group.kube-jenkins-tg.arn}"
  target_id = "${aws_instance.kube-node02.id}"
  port = 30808
}

resource "aws_alb_target_group_attachment" "kube-jenkins-tg-attach-node03" {
  target_group_arn = "${aws_alb_target_group.kube-jenkins-tg.arn}"
  target_id = "${aws_instance.kube-node03.id}"
  port = 30808
}

resource "aws_alb_target_group_attachment" "kube-dashboard-tg-attach-node01" {
  target_group_arn = "${aws_alb_target_group.kube-dashboard-tg.arn}"
  target_id = "${aws_instance.kube-node01.id}"
  port = 30808
}

resource "aws_alb_target_group_attachment" "kube-dashboard-tg-attach-node02" {
  target_group_arn = "${aws_alb_target_group.kube-dashboard-tg.arn}"
  target_id = "${aws_instance.kube-node02.id}"
  port = 30808
}

resource "aws_alb_target_group_attachment" "kube-dashboard-tg-attach-node03" {
  target_group_arn = "${aws_alb_target_group.kube-dashboard-tg.arn}"
  target_id = "${aws_instance.kube-node03.id}"
  port = 30808
}

resource "aws_alb_listener_rule" "kube-web-list-rule" {
  listener_arn = "${aws_alb_listener.kube-lb-listener.arn}"
  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.kube-web-tg.arn}"
  }
  condition {
    host_header {
      values = ["webapp.gl-project1.com"]
    }
  }
}

resource "aws_alb_listener_rule" "kube-web-test-list-rule" {
  listener_arn = "${aws_alb_listener.kube-lb-listener.arn}"
  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.kube-web-test-tg.arn}"

  }
  condition {
    host_header {
      values = ["test-webapp.gl-project1.com"]
    }
  }
}

resource "aws_alb_listener_rule" "kube-jenkins-rule" {
  listener_arn = "${aws_alb_listener.kube-lb-listener.arn}"
  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.kube-jenkins-tg.arn}"
  }
  condition {
    host_header {
      values = ["jenkins.gl-project1.com"]
    }
  }
}

resource "aws_alb_listener_rule" "kube-dashboard-rule" {
  listener_arn = "${aws_alb_listener.kube-lb-listener.arn}"
  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.kube-dashboard-tg.arn}"
  }
  condition {
    host_header {
      values = ["dashboard.gl-project1.com"]
    }
  }
}

resource "aws_route53_record" "webapp" {
  name = "webapp.gl-project1.com"
  zone_id = "${var.zone_id}"
  type = "A"

  alias {
    name = "${aws_lb.kube-lb.dns_name}"
    zone_id = "${aws_lb.kube-lb.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "jenkins" {
  name = "jenkins.gl-project1.com"
  zone_id = "${var.zone_id}"
  type = "A"

  alias {
    name = "${aws_lb.kube-lb.dns_name}"
    zone_id = "${aws_lb.kube-lb.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "test" {
  name = "test-webapp.gl-project1.com"
  zone_id = "${var.zone_id}"
  type = "A"

  alias {
    name = "${aws_lb.kube-lb.dns_name}"
    zone_id = "${aws_lb.kube-lb.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "dashboard" {
  name = "dashboard.gl-project1.com"
  zone_id = "${var.zone_id}"
  type = "A"

  alias {
    name = "${aws_lb.kube-lb.dns_name}"
    zone_id = "${aws_lb.kube-lb.zone_id}"
    evaluate_target_health = false
  }
}




